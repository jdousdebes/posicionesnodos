import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';

export interface Position {
    id?: string;
    latitude: string;
    longitude: string;
    altitude: string;
}

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
    private positionsCollection: AngularFirestoreCollection<Position>;

    private positions: Observable<Position[]>;

    constructor(
        public db: AngularFirestore
    ) {
        this.positionsCollection = db.collection<Position>('positions');

        this.positions = this.positionsCollection.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                });
            })
        );
    }

    addUser(value) {
        return new Promise<any>((resolve, reject) => {
            this.db.collection('/positions').add({
                latitude: value.latitude,
                longitude: value.longitude,
                altitude: value.altitude
            })
                .then(
                    (res) => {
                        resolve(res);
                    },
                    err => reject(err)
                );
        });
    }

    getPositions() {
        return this.positions;
    }

    getPosition(id) {
        return this.positionsCollection.doc<Position>(id).valueChanges();
    }

    updatePosition(todo: Position, id: string) {
        return this.positionsCollection.doc(id).update(todo);
    }

    addPosition(todo: Position) {
        return this.positionsCollection.add(todo);
    }

    removePosition(id) {
        return this.positionsCollection.doc(id).delete();
    }

}
