import {Component, OnInit} from '@angular/core';
import {Platform, ToastController} from '@ionic/angular';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FirebaseService} from '../firebase.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {forEach} from '@angular-devkit/schematics';
import {take} from 'rxjs/operators';
import {GoogleMap, GoogleMaps, GoogleMapsAnimation, ILatLng, Marker} from '@ionic-native/google-maps';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
    map: GoogleMap;

    form: FormGroup;
    positions$ = this.firebaseService.getPositions();
    totalPositions: number;

    positionsIds: any[];

    displayedColumns = [
        'latitude',
        'longitude',
        'altitude',
    ];

    constructor(
        private fb: FormBuilder,
        private firebaseService: FirebaseService,
        private toastController: ToastController,
        private geolocation: Geolocation,
        private platform: Platform,
    ) {
    }

    async ngOnInit() {
        this.form = this.fb.group({
            latitude: [''],
            longitude: [''],
            altitude: [''],
        });

        // Since ngOnInit() is executed before `deviceready` event,
        // you have to wait the event.
        await this.platform.ready();
        await this.loadMap();

        let watch = this.geolocation.watchPosition();
        watch.pipe(take(1)).subscribe((resp) => {
            if (resp) {
                this.form.get('latitude').setValue(resp.coords.latitude ? resp.coords.latitude : 'No se pudo recuperar latitud');
                this.form.get('longitude').setValue(resp.coords.longitude ? resp.coords.longitude : 'No se pudo recuperar longitud');
                this.form.get('altitude').setValue(resp.coords.altitude ? resp.coords.altitude : 'No se pudo recuperar altitud');
            }
            // data can be a set of coordinates, or an error (if an error occurred).
            // data.coords.latitude
        });
        // data.coords.longitude
        this.positions$.pipe(take(1)).subscribe( positions => {
            console.log('positions: ', positions);
            this.positionsIds = positions;
            this.totalPositions = positions.length;
            this.addMarkers(positions);
        });


    }

    submit() {
        this.geolocation.getCurrentPosition().then((resp) => {
            console.log('response: ', resp);
            if (resp) {
                this.form.get('latitude').setValue(resp.coords.latitude);
                this.form.get('longitude').setValue(resp.coords.longitude);
                this.form.get('altitude').setValue(resp.coords.altitude);
            }
            console.log(this.form.getRawValue());
            this.firebaseService.addPosition(this.form.getRawValue())
                .then(value => {
                    console.log('value: ', value);
                    this.successToast('Posicion registrada con exito');
                });
        }).catch((error) => {
            console.log('Error getting location', error);
        });
    }

    deleteAll() {
        for (let position of this.positionsIds) {
            console.log(position);
            this.firebaseService.removePosition(position.id);
        }
    }

    async successToast(message?: string) {
        const toast = await this.toastController.create({
            message: message || 'OK',
            duration: 3000,
        });

        toast.present();
    }


    loadMap() {
        this.map = GoogleMaps.create('map_canvas', {
            camera: {
                target: {
                    lat: -24.8063523,
                    lng: -65.4104148
                },
                zoom: 18,
                tilt: 30
            }
        });
    }

    addMarkers(positions: any[]) {
        for (const position of positions) {
            console.log('position: ', position);
            const currentPosition: ILatLng = {
                lat: position.latitude,  // position.coords.latitude,
                lng: position.longitude // position.coords.longitude
            };

            const marker: Marker = this.map.addMarkerSync({
                title: '@ionic-native/google-maps plugin!',
                snippet: 'This plugin is awesome!',
                position: currentPosition,
                animation: GoogleMapsAnimation.BOUNCE
            });
        }
    }
}
