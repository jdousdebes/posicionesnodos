import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FirebaseService} from '../firebase.service';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    GoogleMapOptions,
    CameraPosition,
    MarkerOptions,
    Marker,
    Environment, ILatLng, GoogleMapsAnimation
} from '@ionic-native/google-maps';
import {Platform} from '@ionic/angular';


@Component({
    selector: 'app-list',
    templateUrl: 'list.page.html',
    styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {

    map: GoogleMap;
    positions$ = this.firebaseService.getPositions();

    constructor(
        private firebaseService: FirebaseService,
        private geolocation: Geolocation,
        private platform: Platform,
    ) { }

    async ngOnInit() {
        // Since ngOnInit() is executed before `deviceready` event,
        // you have to wait the event.
        await this.platform.ready();
        await this.loadMap();
        this.positions$.subscribe( positions => {
            console.log('positions: ', positions);
            this.addMarkers(positions);
        });
    }

    loadMap() {
        this.map = GoogleMaps.create('map_canvas', {
            camera: {
                target: {
                    lat: -24.8063523,
                    lng: -65.4104148
                },
                zoom: 18,
                tilt: 30
            }
        });
    }

    addMarkers(positions: any[]) {
        for (const position of positions) {
            console.log('position: ', position);
            const currentPosition: ILatLng = {
                lat: position.latitude,  // position.coords.latitude,
                lng: position.longitude // position.coords.longitude
            };

            const marker: Marker = this.map.addMarkerSync({
                title: '@ionic-native/google-maps plugin!',
                snippet: 'This plugin is awesome!',
                position: currentPosition,
                animation: GoogleMapsAnimation.BOUNCE
            });
        }
    }

}
